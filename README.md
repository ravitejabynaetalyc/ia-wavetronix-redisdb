# IA Wavetronix RedisDB 

This is a in-memory database stores static content like location data, threshold speed values, map of detectors and nearby detectors 

## Getting Started

### Prerequisites
To succesfully run this project, you need JAVA 8, Maven and Spark.
Redis Installation: https://redis.io/topics/quickstart
1. wget http://download.redis.io/redis-stable.tar.gz
2. sudo apt install redis-server
3. nohup redis-server &


### Installing

To run this project on your local machine:

1. git clone https://ravitejabynaetalyc@bitbucket.org/ravitejabynaetalyc/ia-wavetronix-redisdb.git
2. cd ia-wavetronix-redisdb
3. Go to applicaton.properties file in the project's root folder and edit "thresholdDataFileName", "locationDataFileName" and "targetDetectorsFileName"
4. Run "mvn clean install -DskipTests"
5. Open the project in IDE and Run/Debug Run.java for running RedisDB.
6. OR Run "java -jar target/redisdb-1.0-SNAPSHOT.jar" for running RedisDB. 

## Deployment

Currently, this data pipeline is running on aws ubuntu machines.

 - ssh -i "etalyc_camera.pem" ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com

To deploy:

1. Run the project as mentioned above locally and generate the jar file.
2. scp -i "/Users/ravitejarajabyna/Downloads/etalyc_camera.pem" target/wavetronix-redisdb-1.0-SNAPSHOT.jar ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com:redisdb.jar
	scp -i "/Users/ravitejarajabyna/Downloads/etalyc_camera.pem" src/main/resources/feb2019_thres.csv ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com:feb2019_thres.csv
	scp -i "/Users/ravitejarajabyna/Downloads/etalyc_camera.pem" src/main/resources/location.csv ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com:location.csv
3. ssh into the machine (ssh -i "etalyc_camera.pem" ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com)
4. Kill the running jobs for "redisdb.jar"
5. Then run "nohup java -jar redisdb.jar &".

## Debugging
1. Check nohup.out for logs

## Functionality

1. Use threshold file and generate Threshold value for each detector.
2. Use location data file to store location for each detector.
3. Use location data file and store detector and corresponding nearby detectors within 2 miles.


## Authors

* **Raviteja Raja Byna** - *Initial work* - Github (https://github.com/BRaviteja)

## License







