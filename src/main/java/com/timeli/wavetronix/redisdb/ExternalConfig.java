package com.timeli.wavetronix.redisdb;

public class ExternalConfig {

    @Override
    public String toString() {
        return "ExternalConfig{" +
                "thresholdDataFileName='" + thresholdDataFileName + '\'' +
                ", locationDataFileName='" + locationDataFileName + '\'' +
                ", targetDetectorsFileName='" + targetDetectorsFileName + '\'' +
                '}';
    }

    public ExternalConfig(String thresholdDataFileName, String locationDataFileName, String targetDetectorsFileName) {
        this.thresholdDataFileName = thresholdDataFileName;
        this.locationDataFileName = locationDataFileName;
        this.targetDetectorsFileName = targetDetectorsFileName;
    }

    public String getThresholdDataFileName() {
        return thresholdDataFileName;
    }

    public void setThresholdDataFileName(String thresholdDataFileName) {
        this.thresholdDataFileName = thresholdDataFileName;
    }

    public String getLocationDataFileName() {
        return locationDataFileName;
    }

    public void setLocationDataFileName(String locationDataFileName) {
        this.locationDataFileName = locationDataFileName;
    }

    public String getTargetDetectorsFileName() {
        return targetDetectorsFileName;
    }

    public void setTargetDetectorsFileName(String targetDetectorsFileName) {
        this.targetDetectorsFileName = targetDetectorsFileName;
    }

    private String thresholdDataFileName;

    private String locationDataFileName;

    private String targetDetectorsFileName;

    public ExternalConfig() {
    }




}
