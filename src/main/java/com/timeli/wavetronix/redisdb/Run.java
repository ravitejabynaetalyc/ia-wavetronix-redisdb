package com.timeli.wavetronix.redisdb;

import redis.clients.jedis.Jedis;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Properties;

public class Run {

    public static void main(String[] args) throws Exception {
        LoadDataToRedis loaddb = new LoadDataToRedis(getConfigProperties());

        final Jedis jedis = initDb();
        jedis.auth("Clust3rax$");

        loaddb.loadThresholdData(jedis);
        loaddb.loadLocationData(jedis);
        loaddb.loadTargetRoutes(jedis);
        loaddb.loadSegmentGeoData(jedis);

    }

    private static Jedis initDb(){
        return new Jedis("localhost");
    }

    private static ExternalConfig getConfigProperties() throws Exception {
        Properties mainProperties = new Properties();
        mainProperties.load(new FileInputStream("./application.properties"));
        ExternalConfig config = new ExternalConfig();
        config.setLocationDataFileName(mainProperties.getProperty("locationDataFileName"));
        config.setTargetDetectorsFileName(mainProperties.getProperty("targetDetectorsFileName"));
        config.setThresholdDataFileName(mainProperties.getProperty("thresholdDataFileName"));
        return config;
    }
}
